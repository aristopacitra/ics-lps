import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser(GlobalVariable.url)

WebUI.maximizeWindow()

CustomKeywords.'customKeyword.utilities.logintoapps'(GlobalVariable.username, GlobalVariable.password)

WebUI.verifyElementPresent(findTestObject('Fitur Umum/Case Management/menuFiturUmum'), 0)

WebUI.click(findTestObject('Fitur Umum/Case Management/menuFiturUmum'))

WebUI.verifyElementPresent(findTestObject('Fitur Umum/Case Management/menuTrackingCase'), 0)

WebUI.click(findTestObject('Fitur Umum/Case Management/menuTrackingCase'))

WebUI.verifyElementPresent(findTestObject('Fitur Umum/Case Management/actionButton(1)'), 0)

WebUI.click(findTestObject('Fitur Umum/Case Management/actionButton(1)'))

WebUI.verifyElementPresent(findTestObject('Fitur Umum/Case Management/ubahButton'), 0)

WebUI.click(findTestObject('Fitur Umum/Case Management/ubahButton'))

WebUI.verifyElementPresent(findTestObject('Fitur Umum/Case Management/textUbahCM'), 0)

WebUI.verifyElementPresent(findTestObject('Fitur Umum/Case Management/fieldNamaCase'), 0)

WebUI.click(findTestObject('Fitur Umum/Case Management/fieldNamaCase'))

def namaCaseManagement = 'Testing edit via Katalon ' + CustomKeywords.'customKeyword.utilities.randomString'(5)

WebUI.setText(findTestObject('Fitur Umum/Case Management/fieldNamaCase'), namaCaseManagement)

WebUI.verifyElementPresent(findTestObject('Fitur Umum/Case Management/fieldDeskripsi'), 0)

WebUI.click(findTestObject('Fitur Umum/Case Management/fieldDeskripsi'))

WebUI.setText(findTestObject('Fitur Umum/Case Management/fieldDeskripsi'), 'Testing edit deskripsi via Katalon ya')

WebUI.verifyElementPresent(findTestObject('Fitur Umum/Case Management/lowButton'), 0)

WebUI.click(findTestObject('Fitur Umum/Case Management/lowButton'))

WebUI.verifyElementPresent(findTestObject('Fitur Umum/Case Management/buttonSimpan'), 0)

WebUI.click(findTestObject('Fitur Umum/Case Management/buttonSimpan'))

WebUI.verifyElementPresent(findTestObject('Fitur Umum/Case Management/buttonSimpanPopupKonfirmasi'), 0)

WebUI.click(findTestObject('Fitur Umum/Case Management/buttonSimpanPopupKonfirmasi'))

WebUI.delay(3)

WebUI.closeBrowser()

