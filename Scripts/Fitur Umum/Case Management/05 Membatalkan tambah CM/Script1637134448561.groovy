import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser(GlobalVariable.url)

WebUI.maximizeWindow()

CustomKeywords.'customKeyword.utilities.logintoapps'(GlobalVariable.username, GlobalVariable.password)

//WebUI.verifyElementPresent(findTestObject('Fitur Umum/Login/fieldUsername'), 0)
//WebUI.setText(findTestObject('Fitur Umum/Login/fieldUsername'), 'superadmin@email.com')
//WebUI.verifyElementPresent(findTestObject('Fitur Umum/Login/buttonLanjutkan'), 0)
//WebUI.click(findTestObject('Fitur Umum/Login/buttonLanjutkan'))
//WebUI.verifyElementPresent(findTestObject('Fitur Umum/Login/fieldPassword'), 0)
//WebUI.setEncryptedText(findTestObject('Fitur Umum/Login/fieldPassword'), 'iFGeFYmXIrUhQZHvW7P22w==')
//WebUI.verifyElementPresent(findTestObject('Fitur Umum/Login/buttonSignIn'), 0)
//WebUI.click(findTestObject('Fitur Umum/Login/buttonSignIn'))
WebUI.verifyElementPresent(findTestObject('Fitur Umum/Case Management/menuFiturUmum'), 0)

WebUI.click(findTestObject('Fitur Umum/Case Management/menuFiturUmum'))

WebUI.verifyElementPresent(findTestObject('Fitur Umum/Case Management/menuTrackingCase'), 0)

WebUI.click(findTestObject('Fitur Umum/Case Management/menuTrackingCase'))

WebUI.verifyElementPresent(findTestObject('Fitur Umum/Case Management/buttonTambahCM'), 0)

WebUI.click(findTestObject('Fitur Umum/Case Management/buttonTambahCM'))

WebUI.verifyElementPresent(findTestObject('Fitur Umum/Case Management/fieldNamaCase'), 0)

WebUI.setText(findTestObject('Fitur Umum/Case Management/fieldNamaCase'), 'Testing tambah via Katalon')

WebUI.verifyElementPresent(findTestObject('Fitur Umum/Case Management/fieldDeskripsi'), 0)

WebUI.setText(findTestObject('Fitur Umum/Case Management/fieldDeskripsi'), 'Testing ini deskripsi ya')

WebUI.verifyElementPresent(findTestObject('Fitur Umum/Case Management/highButton'), 0)

WebUI.click(findTestObject('Fitur Umum/Case Management/highButton'))

WebUI.verifyElementPresent(findTestObject('Fitur Umum/Case Management/buttonBatal'), 0)

WebUI.click(findTestObject('Fitur Umum/Case Management/buttonBatal'))

WebUI.verifyElementPresent(findTestObject('Fitur Umum/Case Management/textCaseManagement'), 0)

WebUI.delay(2)

WebUI.closeBrowser()

