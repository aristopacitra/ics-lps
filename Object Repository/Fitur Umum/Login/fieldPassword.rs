<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>fieldPassword</name>
   <tag></tag>
   <elementGuidId>4a706389-c0ba-4839-b8f5-c209372b8b34</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@formcontrolname = 'PasswordHash']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>formcontrolname</name>
      <type>Main</type>
      <value>PasswordHash</value>
   </webElementProperties>
</WebElementEntity>
