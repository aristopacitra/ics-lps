<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>statusWaiting</name>
   <tag></tag>
   <elementGuidId>9ea71805-edf3-4c62-bfe0-3a178c1e743b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Waiting Respond' or . = 'Waiting Respond')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Waiting Respond</value>
   </webElementProperties>
</WebElementEntity>
