<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>lastPageButton</name>
   <tag></tag>
   <elementGuidId>c3e8c78d-7465-48e9-a697-b2831509da0c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@type = 'button' and @class = 'p-ripple p-element p-paginator-last p-paginator-element p-link ng-star-inserted']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>p-ripple p-element p-paginator-last p-paginator-element p-link ng-star-inserted</value>
   </webElementProperties>
</WebElementEntity>
